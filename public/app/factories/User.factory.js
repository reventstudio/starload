angular.module('StarLoad').factory("User", function ($http) {
    User = function () {
        this.userId = 0;
        this.username = "Anonymous";
        this.displayName = "Anonymous Group";
        this.dateRegistered = new Date();
        this.email = "";
        this.signature = "";
        this.organization = null;
        
        this.getOrganization = function () {
            return this.organization;
        };
        
        this.setOrganization = function (_org) {
            this.organization = _org;
        };
        
        this.getUserId = function () {
            return this.userId;
        };
        
        this.getUsername = function(){
            return this.username;
        };
        
        this.getDisplayName = function () {
            return this.displayName;
        };
        
        this.getEmail = function () {
            return this.email;
        };
        
        this.getSignature = function () {
            return this.signature;
        };
        
        this.getDateRegistered = function () {
            return this.dateRegistered;
        };
        
        this.setUserId = function (_id) {
            this.userId = _id;
        };
        
        this.setUsername = function (_username) {
            this.username = _username;
        };
        
        this.setDisplayName = function(_dpName){
            this.displayName = _dpName;
        };
        
        this.setEmail = function (_email) {
            this.email = _email;
        };
        
        this.setSignature = function (_sig) {
            this.signature = _sig;
        };
        
        this.setDateRegistered = function (_dateReg) {
            this.dateRegistered = _dateReg;
        };
        
        this.getUserFromDbById = function (_id) {
            $http.get('/api/v1/users/' + _id, {cache: true}).then(function (response) {
               if(response.data.length == 0){
                   return { error: "User has not been found" };
               }else{
                   return response.data[0];
               }
            });
        };
        
        this.updateDisplayNameDb = function (_id) {
            $http.patch('/api/v1/users/', {id: _id, displayName: this.getDisplayName()}).then(function (response) {
                console.debug(response);
            });
        };
    
        this.updateEmailDb = function (_id) {
            $http.patch('/api/v1/users/', {id: _id, email: this.getEmail()}).then(function (response) {
                console.debug(response);
            });
        };
    
        this.updateSignatureDb = function (_id) {
            $http.patch('/api/v1/users/', {id: _id, signature: this.getSignature()}).then(function (response) {
                console.debug(response);
            });
        };
    
        this.updatePasswordDb = function (_id, _pass) {
            $http.patch('/api/v1/users/', {id: _id, password: _pass}).then(function (response) {
                console.debug(response);
            });
        };
        
        return this;
        
    };
    
    return User;
});