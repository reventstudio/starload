angular.module('StarLoad').factory("Load", function ($http) {
    Load = function () {
        this.loadId = 0;
        this.load =  "Untitled Load";
        this.loadDescription =  "Safe Travels!";
        this.senderId = "Planet Express";
        this.receiverId = "";
        this.pickupLocation = null;
        this.dropOffLocation = null;
        this.startingBid = 0;
        this.finalBid = 0;
        this.loadContents = [];
        this.pickupDate = new Date();
        this.dropOffDate = new Date(this.pickupDate.getDate() + 7);
        
        /** Load Contents **/
        
        /**
         * {
         *      item: "",
         *      quantity: 0
         * }
         */
        
        //Getters
        
        this.getLoadId = function () {
            return this.loadId;
        };
        
        this.getLoadName = function () {
            return this.load;
        };
        
        this.getLoadDescription = function () {
            return this.loadDescription;
        };
        
        this.getSenderId = function () {
            return this.senderId;
        };
        
        this.getReceiverId = function () {
            return this.receiverId;
        };
        
        this.getPickupLocation = function () {
            return this.pickupLocation;
        };
        
        this.getDropOffLocation = function () {
            return this.dropOffLocation;
        };
        
        this.getStartingBid = function () {
            return this.startingBid;
        };
        
        this.getFinalBid = function () {
            return this.finalBid;
        };
        
        this.getLoadContents = function () {
            return this.loadContents;
        };
        
        this.getPickUpDate = function () {
            return this.pickupDate;
        };
        
        this.getDropOffDate = function () {
            return this.dropOffDate;
        };
        
        // Setters
        
        this.setLoadId = function(_loadId){
            this.loadId = _loadId;
        };
        
        this.setLoadName = function (_loadName) {
            this.load = _loadName;
        };
        
        this.setLoadDescription = function (_loadDesc) {
            this.loadDescription = _loadDesc;
        };
        
        this.setSender = function (_senderId) {
            this.senderId = _senderId;
        };
        
        this.setReceiver = function (_receiverId) {
            this.receiverId = _receiverId;
        };
        
        this.setPickUpLocation = function (_pickup) {
            this.pickupLocation = _pickup;
        };
        
        this.setDropOffLocation = function (_dropOff) {
            this.dropOffLocation = _dropOff;
        };
        
        this.setStartingBid = function (_startingBid) {
            this.startingBid = _startingBid;
        };
        
        this.setCargoLoad = function (_cargo) {
            this.loadContents = _cargo;
        };
        
        this.setFinalBid = function (_finalBid) {
            this.finalBid = _finalBid;
        };
        
        this.setPickUpDate = function (_date) {
            this.pickupDate = _date;
        };
        
        this.setDropOffDate = function (_date) {
            this.dropOffDate = _date;
        };
        
        this.addItem = function (_item) {
            this.loadContents.push(_item);
        };
        
        this.removeItem = function (_item) {
            var index = this.loadContents.indexOf(_item);
            this.loadContents.splice(index, 1);
        };
        
        this.fetchDBLoadById = function (_loadId) {
            return $http.get('/api/v1/loads/' + _loadId, {cache:false}).then(function (response) {
               return response.data[0];
            }).catch(function(error){
                console.log(error);
                return false;
            });
        };
        
        this.saveLoadToDB = function () {
            
        };
                                
        return this;
    };
    
    return Load;
});