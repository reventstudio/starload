angular.module('StarLoad').factory("Auth", function ($http) {
    Auth = function(_user, _pass){
        this.username = _user | "";
        this.password = _pass | "";
        this.loggedIn = false;
        this.active = false;
        
        this.getUsername = function () {
            return this.username;
        };
        
        this.getPassword = function () {
            return this.password;
        };
        
        this.isLoggedIn = function () {
            return this.loggedIn;
        };
        
        this.isActive = function () {
            return this.active;
        };
        
        this.setUsername = function (_user) {
            this.username = _user;
        };
        
        this.setPassword = function (_pass) {
            this.password = _pass;
        };
        
        this.setLogStatus = function (_loggedIn) {
            this.loggedIn = _loggedIn
        };
        
        this.logIn = function () {
            var data = {
                username : this.getUsername(),
                password : this.getPassword()
            };
            
            $http.post('/api/v1/auth', data, {cache: false}).then(function (response) {
                if(response.data.length == 0 || !response.data[0].isActive()){
                    this.authFailed();
                }else{
                    this.authSuccess(response.data[0]);
                }
            });
        };
        
        this.authFailed = function () {
            return {
                error : "Incorrect password, username or the user has not been activated in our system"
            }
        };
        
        this.authSuccess = function (data) {
            return data;
        };
        
        return this;
    };
    
    return Auth;
});