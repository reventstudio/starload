 angular.module('StarLoad', ['ui.router', 'ngMaterial', 'ngMessages', 'ngAnimate']);
 
 angular.module('StarLoad').config(function ($stateProvider, $urlRouterProvider) {
     $stateProvider
         .state('about',{
             url: '/',
             controller: 'aboutController',
             templateUrl: "app/partials/about.partial.html"
         })
         .state('error', {
             url: '/error/{errorCode}',
             controller: 'errorController',
             templateUrl: "app/partials/error.partial.html"
         })
         .state('loads',{
             url: '/loads',
             controller: 'rootLoadController',
             templateUrl: 'app/partials/root.loads.partial.html'
         })
         .state('loads.load',{
             url: '/view/{loadId}',
             controller: 'loadController',
             templateUrl: 'app/partials/load.loads.partial.html'
         })
         .state('loads.loads',{
             url: '/all',
             controller: 'loadsController',
             templateUrl: 'app/partials/loads.loads.partial.html'
         })
         .state('loads.create', {
             url: '/create/{receivingPartyID}',
             controller: 'createLoadController',
             templateUrl: 'app/partials/create.load.partial.html'
         })
         .state('loads.active',{
             url: '/active',
             controller: 'activeLoadController',
             templateUrl: 'app/partials/active.load.partial.html'
         });
    
     $urlRouterProvider.otherwise('/');
 });

 angular.module("StarLoad").run(function ($rootScope) {
 });

 angular.module('StarLoad').config(function($mdThemingProvider) {
     $mdThemingProvider.theme('starload')
                       .primaryPalette('grey', {
                           default: '900'
                       })
                       .accentPalette('red');
    
     $mdThemingProvider.setDefaultTheme('starload');
 });