angular.module("StarLoad").controller("mainController", function($scope, $state, Load){
    $scope.loadList = [];
    
    var mochData = [{
        "loadName": "Twimbo",
        "sendingParty": "Fadel, Christiansen and O'Hara",
        "pickupLocation": "Greece",
        "dropoffLocation": "China",
        "bid": 10,
        "notes": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "cargoItems": "Water"
    }, {
        "loadName": "Browsetype",
        "sendingParty": "Rowe, Sipes and Rath",
        "pickupLocation": "China",
        "dropoffLocation": "Palestinian Territory",
        "bid": 98,
        "notes": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "cargoItems": "apples"
    }, {
        "loadName": "Jaloo",
        "sendingParty": "Lueilwitz-Hagenes",
        "pickupLocation": "Syria",
        "dropoffLocation": "Indonesia",
        "bid": 99,
        "notes": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
        "cargoItems": "apples"
    }, {
        "loadName": "Kazu",
        "sendingParty": "Reilly Inc",
        "pickupLocation": "Russia",
        "dropoffLocation": "France",
        "bid": 56,
        "notes": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Yodo",
        "sendingParty": "Bosco Group",
        "pickupLocation": "Japan",
        "dropoffLocation": "Belarus",
        "bid": 46,
        "notes": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.",
        "cargoItems": "Water"
    }, {
        "loadName": "Jaxnation",
        "sendingParty": "Pfeffer, Glover and Legros",
        "pickupLocation": "Indonesia",
        "dropoffLocation": "China",
        "bid": 57,
        "notes": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
        "cargoItems": "apples"
    }, {
        "loadName": "Fiveclub",
        "sendingParty": "Lindgren, Swaniawski and Grady",
        "pickupLocation": "United States",
        "dropoffLocation": "Japan",
        "bid": 62,
        "notes": "In congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
        "cargoItems": "juice"
    }, {
        "loadName": "Twitterbeat",
        "sendingParty": "Goyette-Frami",
        "pickupLocation": "Brazil",
        "dropoffLocation": "Costa Rica",
        "bid": 21,
        "notes": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Yombu",
        "sendingParty": "Wuckert, Murazik and Schulist",
        "pickupLocation": "Indonesia",
        "dropoffLocation": "Poland",
        "bid": 40,
        "notes": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Buzzbean",
        "sendingParty": "Hegmann-Mraz",
        "pickupLocation": "Bangladesh",
        "dropoffLocation": "Canada",
        "bid": 66,
        "notes": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.",
        "cargoItems": "juice"
    }, {
        "loadName": "Thoughtstorm",
        "sendingParty": "Schuppe, Fay and Kohler",
        "pickupLocation": "Guatemala",
        "dropoffLocation": "Armenia",
        "bid": 7,
        "notes": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Katz",
        "sendingParty": "Rolfson, Hagenes and Fritsch",
        "pickupLocation": "Indonesia",
        "dropoffLocation": "Brazil",
        "bid": 13,
        "notes": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Reallinks",
        "sendingParty": "Goodwin-Mante",
        "pickupLocation": "China",
        "dropoffLocation": "Poland",
        "bid": 20,
        "notes": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "cargoItems": "phones"
    }, {
        "loadName": "Twitterworks",
        "sendingParty": "Thiel and Sons",
        "pickupLocation": "Czech Republic",
        "dropoffLocation": "Sweden",
        "bid": 40,
        "notes": "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\n\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.",
        "cargoItems": "Water"
    }, {
        "loadName": "Quire",
        "sendingParty": "Kub and Sons",
        "pickupLocation": "China",
        "dropoffLocation": "Greece",
        "bid": 84,
        "notes": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
        "cargoItems": "juice"
    }, {
        "loadName": "Wordware",
        "sendingParty": "Jakubowski LLC",
        "pickupLocation": "Poland",
        "dropoffLocation": "Malaysia",
        "bid": 23,
        "notes": "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
        "cargoItems": "Water"
    }, {
        "loadName": "Fivebridge",
        "sendingParty": "Kemmer Group",
        "pickupLocation": "Philippines",
        "dropoffLocation": "South Africa",
        "bid": 62,
        "notes": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Fadeo",
        "sendingParty": "Walsh-Langworth",
        "pickupLocation": "China",
        "dropoffLocation": "Portugal",
        "bid": 49,
        "notes": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
        "cargoItems": "phones"
    }, {
        "loadName": "Leenti",
        "sendingParty": "Walker and Sons",
        "pickupLocation": "Albania",
        "dropoffLocation": "China",
        "bid": 22,
        "notes": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\n\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
        "cargoItems": "juice"
    }, {
        "loadName": "Flipstorm",
        "sendingParty": "Rippin Inc",
        "pickupLocation": "Russia",
        "dropoffLocation": "Ukraine",
        "bid": 38,
        "notes": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
        "cargoItems": "phones"
    }, {
        "loadName": "Browsetype",
        "sendingParty": "Rippin Inc",
        "pickupLocation": "China",
        "dropoffLocation": "Equatorial Guinea",
        "bid": 46,
        "notes": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.",
        "cargoItems": "phones"
    }, {
        "loadName": "Teklist",
        "sendingParty": "Ritchie, Ziemann and Olson",
        "pickupLocation": "Mexico",
        "dropoffLocation": "Indonesia",
        "bid": 50,
        "notes": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.",
        "cargoItems": "apples"
    }, {
        "loadName": "Blogtag",
        "sendingParty": "Schulist-Kassulke",
        "pickupLocation": "China",
        "dropoffLocation": "China",
        "bid": 12,
        "notes": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
        "cargoItems": "apples"
    }, {
        "loadName": "Riffwire",
        "sendingParty": "Schimmel-Senger",
        "pickupLocation": "China",
        "dropoffLocation": "Israel",
        "bid": 7,
        "notes": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
        "cargoItems": "phones"
    }, {
        "loadName": "Jaloo",
        "sendingParty": "Marks, Wilderman and Miller",
        "pickupLocation": "China",
        "dropoffLocation": "Poland",
        "bid": 95,
        "notes": "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Voonder",
        "sendingParty": "Collins-Schoen",
        "pickupLocation": "Indonesia",
        "dropoffLocation": "China",
        "bid": 69,
        "notes": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
        "cargoItems": "Water"
    }, {
        "loadName": "Youspan",
        "sendingParty": "Schmidt LLC",
        "pickupLocation": "China",
        "dropoffLocation": "China",
        "bid": 10,
        "notes": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        "cargoItems": "phones"
    }, {
        "loadName": "Browsebug",
        "sendingParty": "O'Hara, Champlin and O'Reilly",
        "pickupLocation": "China",
        "dropoffLocation": "Russia",
        "bid": 35,
        "notes": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "cargoItems": "phones"
    }, {
        "loadName": "Rhynoodle",
        "sendingParty": "Stanton, Wuckert and Jaskolski",
        "pickupLocation": "Philippines",
        "dropoffLocation": "Portugal",
        "bid": 1,
        "notes": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\n\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
        "cargoItems": "apples"
    }, {
        "loadName": "Snaptags",
        "sendingParty": "Ortiz, Stark and Gleichner",
        "pickupLocation": "Russia",
        "dropoffLocation": "Philippines",
        "bid": 23,
        "notes": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
        "cargoItems": "monster drink"
    }, {
        "loadName": "Skyba",
        "sendingParty": "Hauck, Beatty and Nikolaus",
        "pickupLocation": "Guatemala",
        "dropoffLocation": "Japan",
        "bid": 13,
        "notes": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "cargoItems": "apples"
    }, {
        "loadName": "Riffpedia",
        "sendingParty": "Eichmann-Hills",
        "pickupLocation": "Philippines",
        "dropoffLocation": "South Africa",
        "bid": 72,
        "notes": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.",
        "cargoItems": "phones"
    }, {
        "loadName": "InnoZ",
        "sendingParty": "Mante, Howe and Witting",
        "pickupLocation": "Philippines",
        "dropoffLocation": "Indonesia",
        "bid": 24,
        "notes": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
        "cargoItems": "juice"
    }, {
        "loadName": "Livetube",
        "sendingParty": "Bayer-Kulas",
        "pickupLocation": "Germany",
        "dropoffLocation": "China",
        "bid": 91,
        "notes": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
        "cargoItems": "monster drink"
    }];
    
    for(var i = 0; i < mochData.length; i++){
        var load = new Load();
        load.setLoadName(mochData[i].loadName);
        load.setSender(mochData[i].sendingParty);
        load.setPickUpLocation(mochData[i].pickupLocation);
        load.setDropOffLocation(mochData[i].dropoffLocation);
        load.setStartingBid(mochData[i].bid);
        load.setLoadDescription(mochData[i].notes);
        load.setCargoLoad([mochData[i].cargoItems]);
        $scope.loadList.push(load);
    }
});