angular.module('StarLoad').controller("rootLoadController", function ($scope, $state) {
        
    $scope.saveLoad = function(_load){
        $scope.loadList.push(_load);
    };
    
    $scope.removeLoad = function (_load) {
        var loadLocationIndex = $scope.loadList.indexOf(_load);
        $scope.loadList.splice(loadLocationIndex, 1);
    };
    
    $scope.getLoad = function (_load) {
        var loadLocationIndex = $scope.loadList.indexOf(_load);
        return $scope.loadList[loadLocationIndex];
    };
    
    $scope.clearLoadList = function () {
        $scope.loadList = [];
    };
    
    $scope.activeClass = function (_state) {
        return (_state == $state.current.name) ? 'md-accent' : 'md-primary';
    };
});