angular.module('StarLoad').controller("createLoadController", function ($scope, Load, $state, $stateParams, $mdConstant) {
    $scope.load = new Load();
    $scope.load.setReceiver($stateParams.receivingPartyID || null);
    
    $scope.addLoad = function () {
        console.log($scope.load);
        $scope.saveLoad($scope.load);
        $state.transitionTo('loads.loads');
    };
    
    $scope.keys =[$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA];
});