angular.module('StarLoad').controller("errorController", function ($scope, $stateParams) {
    $scope.error = $stateParams.errorCode;
    $scope.errorText = "";
    
    switch ($scope.error){
        case 404:
            $scope.errorText = "Page not found";
            break;
        default:
            $scope.errorText = "Page not found";
    }
});